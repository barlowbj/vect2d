/*  Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package vect2d

import (
	"math"
)

// DotProduct calculates the dot product between 2d vectors a and b
func DotProduct(a, b [2]float64) float64 {
	return (a[0] * b[0]) + (a[1] * b[1])
}

// Magnitude returns the magnitude of a 2d vector a.
func Magnitude(a [2]float64) float64 {
	return math.Sqrt(DotProduct(a, a))
}

// GetAngle returns the cos(angle) between two 2d vectors a and b
func GetAngle(a, b [2]float64) float64 {
	return DotProduct(a, b) / (Magnitude(a) * Magnitude(b))
}

// Subtract returns difference between 2d vectors a - b
func Subtract(a, b [2]float64) (result [2]float64) {
	result[0] = a[0] - b[0]
	result[1] = a[1] - b[1]
	return result
}

// Add returns the sum of 2d vector a + b
func Add(a, b [2]float64) (result [2]float64) {
	result[0] = a[0] + b[0]
	result[1] = a[1] + b[1]
	return result
}

// ScalerMult returns the 2d result of multiplying a 2d vector a times a scalar b.
func ScalerMult(a [2]float64, b float64) [2]float64 {
	var result [2]float64
	result[0] = a[0] * b
	result[1] = a[1] * b
	return result
}

// Normalize returns the normalized 2d vector when given a 2d vector a.
func Normalize(a [2]float64) [2]float64 {
	return ScalerMult(a, float64(1)/Magnitude(a))
}
